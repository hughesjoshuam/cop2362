﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using static System.Console;

namespace _20_1_LinqToXmlConstructor
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument xdoc = new XDocument(
               new XElement("customers",
                   new XElement("customer",
                       new XAttribute("ID", "A"),
                       new XAttribute("City", "New York"),
                       new XAttribute("Region", "North America"),
                       new XElement("order",
                           new XAttribute("Item", "Tire"),
                           new XAttribute("Price", 200)
                       )
                   ),
                    new XElement("customer",
                       new XAttribute("ID", "B"),
                       new XAttribute("City", "Mubai"),
                       new XAttribute("Region", "Asia"),
                       new XElement("order",
                           new XAttribute("Item", "Oven"),
                           new XAttribute("Price", 501)
                       )
                   ),
                    //my taking a bash @ it.
                    new XElement("customer",
                       new XAttribute("ID", "C"),
                       new XAttribute("City", "Italy"),
                       new XAttribute("Region", "Italy"),
                       new XElement("order",
                           new XAttribute("Item", "Ferari"),
                           new XAttribute("Price", 98501)
                       ),
                       new XElement("order",
                           new XAttribute("Item", "Corvette"),
                           new XAttribute("Price", 78499)
                       ), new XElement("order",
                           new XAttribute("Item", "F150 Platinum"),
                           new XAttribute("Price", 96859)
                       ), new XElement("order",
                           new XAttribute("Item", "VW Bus"),
                           new XAttribute("Price", 18491)
                       )
                   )
                )
            );
            WriteLine(xdoc);
            Write("Program finished, press Enter/Return to continue:");
            ReadLine();
        }
    }
}
