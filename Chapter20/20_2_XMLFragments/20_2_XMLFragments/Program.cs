﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using static System.Console;

namespace _20_2_XMLFragments
{
    class Program
    {
        static void Main(string[] args)
        {
            XElement xcust = new XElement(
                  new XElement("customers",
                      new XElement("customer",
                          new XAttribute("ID", "A"),
                          new XAttribute("City", "New York"),
                          new XAttribute("Region", "North America"),
                          new XElement("order",
                              new XAttribute("Item", "Tire"),
                              new XAttribute("Price", 200)
                          )
                      ),
                       new XElement("customer",
                          new XAttribute("ID", "B"),
                          new XAttribute("City", "Mubai"),
                          new XAttribute("Region", "Asia"),
                          new XElement("order",
                              new XAttribute("Item", "Oven"),
                              new XAttribute("Price", 501)
                          )
                      ),
                       //my taking a bash @ it.
                       new XElement("customer",
                          new XAttribute("ID", "C"),
                          new XAttribute("City", "Italy"),
                          new XAttribute("Region", "Italy"),
                          new XElement("order",
                              new XAttribute("Item", "Ferari"),
                              new XAttribute("Price", 98501)
                          ),
                          new XElement("order",
                              new XAttribute("Item", "Corvette"),
                              new XAttribute("Price", 78499)
                          ), new XElement("order",
                              new XAttribute("Item", "F150 Platinum"),
                              new XAttribute("Price", 96859)
                          ), new XElement("order",
                              new XAttribute("Item", "VW Bus"),
                              new XAttribute("Price", 18491)
                          )
                      ),
                       //i'm getting hungry...
                       new XElement("customer",
                          new XAttribute("ID", "D"),
                          new XAttribute("City", "Port Charlotte"),
                          new XAttribute("Region", "Florida"),
                          new XElement("order",
                              new XAttribute("Item", "Pizza"),
                              new XAttribute("Price", 19)
                          ),
                          new XElement("order",
                              new XAttribute("Item", "Sub"),
                              new XAttribute("Price", 9)
                          ), 
                          new XElement("order",
                              new XAttribute("Item", "Salad"),
                              new XAttribute("Price", 5)
                          )
                      )
                   )
               );
            string xmlFileName = @"C:\Development\COP2362\Chapter20\20_2_XMLFragments\fragment.xml";
            xcust.Save(xmlFileName);
            XElement xcust2 = XElement.Load(xmlFileName);
            WriteLine("Contents of xcust2");
            WriteLine(xcust2);
            Write("Program finished, press Enter/Return to continue:");
            ReadLine();
        }
    }
}
