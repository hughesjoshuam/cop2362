﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


using static System.Console;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //  Create LINQ structures

                string xmlProducts = @"C:\Development\COP2362\Chapter20\LINQ On Your Own\LINQ\bin\Debug\products.xml";
                string xmlCategories = @"C:\Development\COP2362\Chapter20\LINQ On Your Own\LINQ\bin\Debug\categories.xml";

                //  Retrieve XML from XML documents into XElement
                XDocument xProducts = XDocument.Load(xmlProducts);
                XDocument xCategories = XDocument.Load(xmlCategories);

                //  Create products structure from XElement
                var products = from p in xProducts.Descendants("product")
                               select new
                               {
                                   productID = p.Element("productID").Value,
                                   categoryID = p.Element("categoryID").Value,
                                   productCode = p.Element("productCode").Value,
                                   productName = p.Element("productName").Value,
                                   listPrice = p.Element("listPrice").Value
                               };

                //  Create categories structure from XElement
                var categories = from c in xCategories.Descendants("category")
                                 select new
                                 {
                                     categoryID = c.Element("categoryID").Value,
                                     categoryName = c.Element("categoryName").Value
                                 };

                //  Join products and catories structures using LINQ query
                var queryResults = from p in products
                                   join c in categories on p.categoryID equals c.categoryID
                                   select new { p.productName, c.categoryName, p.listPrice };

            //  Sort by List Price ascending and descending (two outputs) with no other filters

                var querySortAscending = queryResults.OrderBy(l => float.Parse(l.listPrice));
                var querySortDescending = queryResults.OrderByDescending(l => float.Parse(l.listPrice));

                WriteLine("Query Sorted by List Price Ascending\n");
                WriteLine("Product Name, Category Name, List Price\n");
            
                foreach(var item in querySortAscending)
                {
                    WriteLine(item.productName + ", " + item.categoryName + ", " + item.listPrice);
                }

                ReadKey();

                WriteLine("\n\nQuery Sorted by List Price Descending\n");
                WriteLine("Product Name, Category Name, List Price\n");

                foreach (var item in querySortDescending)
                {
                    WriteLine(item.productName + ", " + item.categoryName + ", " + item.listPrice);
                }

                ReadKey();

            //  Display only products in the bass or guitar category

                var queryFilteredResults = queryResults.Where(c => c.categoryName == "Basses" || c.categoryName == "Guitars");

                WriteLine("\n\nProducts in the bass or guitar category\n");
                WriteLine("Product Name, Category Name, List Price\n");

                foreach (var item in queryFilteredResults)
                {
                    WriteLine(item.productName + ", " + item.categoryName + ", " + item.listPrice);
                }

                ReadKey();

            //  Display the total of all the list prices
            
                WriteLine($"\n\nTotal of all list prices: {queryResults.Select(l => l.listPrice).Sum(l => float.Parse(l))}\n");
            
                ReadKey();

            //  Display the total of the list price for each category
            
                //  Foreach category determine total of list prices
                foreach (string category in queryResults.Select(c => c.categoryName).Distinct())
                {
                    WriteLine($"\n\ntotal of all list prices in {category}: {queryResults.Where(c => c.categoryName == category).Select(l => l.listPrice).Sum(l => float.Parse(l))}\n");
                }

                ReadKey();
        }
    }
}
