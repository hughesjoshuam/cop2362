﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace _20_4_MethodSyntax
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = { "Al", "Zen", "Smith", "Jones", "Smythe", "Small", "Ruiz", "Hsieh", "Jorgenson", "Ilyich", "Singh", "Samba", "Fatimah" };
            //var queryResults = names.Where(s => s.StartsWith("S"));   
            
            /*
             * Adding the OrderBy using LINQ Method syntax
             */       
            var queryResults = names.Where(s => s.StartsWith("S")).OrderBy(n => n);
            //var queryResults = names.Where(s => s.StartsWith("S")).OrderByDescending(n => n);

            WriteLine("Names beginning with S:");
            foreach (var item in queryResults)
            {
                WriteLine(item);
            }
            Write("Program finished, press Enter/Return to continue:");
            ReadLine();
        }
    }
}
