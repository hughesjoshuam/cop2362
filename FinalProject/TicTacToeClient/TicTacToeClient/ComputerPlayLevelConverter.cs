﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TicTacToeClient
{
    public enum ComputerPlayLevels
    {
        Easy,
        Medium,
        Difficult,
        Learning
    }

    [ValueConversion(typeof(ComputerPlayLevels), typeof(string))]
    class ComputerPlayLevelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strParameter = parameter.ToString();
            if (strParameter == "easyComputerPlayLevelRadioButton" && (ComputerPlayLevels)value == ComputerPlayLevels.Easy)
                return true;
            else if (strParameter == "mediumComputerPlayLevelRadioButton" && (ComputerPlayLevels)value == ComputerPlayLevels.Medium)
                return true;
            else if (strParameter == "difficultComputerPlayLevelRadioButton" && (ComputerPlayLevels)value == ComputerPlayLevels.Difficult)
                return true;
            else if (strParameter == "learningComputerPlayLevelRadioButton" && (ComputerPlayLevels)value == ComputerPlayLevels.Learning)
                return true;
            return false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strParameter = parameter.ToString();
            string strValue = value.ToString();
            if (strParameter == "easyComputerPlayLevelRadioButton" && (bool)value == true)
                return ComputerPlayLevels.Easy;
            else if (strParameter == "mediumComputerPlayLevelRadioButton" && (bool)value == true)
                return ComputerPlayLevels.Medium;
            else if (strParameter == "difficultComputerPlayLevelRadioButton" && (bool)value == true)
                return ComputerPlayLevels.Difficult;
            else if (strParameter == "learningComputerPlayLevelRadioButton" && (bool)value == true)
                return ComputerPlayLevels.Learning;
            return false;
        }
    }
}
