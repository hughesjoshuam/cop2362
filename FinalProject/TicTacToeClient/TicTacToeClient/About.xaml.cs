﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TicTacToeClient
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            timer.Tick += ((sender, e) =>
            {
                AboutScrollViewerTextBlock.Height += 5;

                if (AboutScrollViewer.VerticalOffset == AboutScrollViewer.ScrollableHeight)
                {
                    AboutScrollViewer.ScrollToEnd();
                }
            });
            timer.Start();
        }
        public void ShowAboutCommand(object sender, ExecutedRoutedEventArgs e)
        {
            var dialog = new About();
            dialog.ShowDialog();
        }
    }
}
