﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace TicTacToeClient
{
    [Serializable]
    public class GameModel
    {
        // Set Property Event Handler
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        //Set Options RoutedCommands
        public static RoutedCommand SaveGameRoutedCommand = new RoutedCommand("Save Game", typeof(GameModel), new InputGestureCollection(new List<InputGesture> { new KeyGesture(Key.O, ModifierKeys.Control) }));
        public static RoutedCommand OpenGameRoutedCommand = new RoutedCommand("Open Game", typeof(GameModel), new InputGestureCollection(new List<InputGesture> { new KeyGesture(Key.O, ModifierKeys.Control) }));

        // Game Started property
        private bool _gameStarted;
        public bool GameStarted
        {
            get { return _gameStarted; }
            set
            {
                _gameStarted = value;
                OnPropertyChanged(nameof(GameStarted));
            }
        }

        // Current Player Property
        private int _currentPlayer;
        public int CurrentPlayer
        {
            get { return _currentPlayer; }
            set
            {
                _currentPlayer = value;
                OnPropertyChanged(nameof(CurrentPlayer));
            }
        }

        // Game State Property
        private string[] _gameState;    
        public string[] GameState
        {
            get { return _gameState; }
            set
            {
                _gameState = value;
                OnPropertyChanged(nameof(GameState));
            }
        }
        public void Save()
        {
            using (var stream = File.Open("GameModel.xml", FileMode.Create))
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(GameModel));
                    serializer.Serialize(stream, this);
                }
                catch (System.IO.IOException e)
                {
                    MessageBox.Show(e.Source + ": " + e.InnerException);
                }

            }
        }
        public static GameModel Create()
        {
            if (File.Exists("GameModel.xml"))
            {
                using (var stream = File.OpenRead("GameModel.xml"))
                {
                    try
                    {
                        var serializer = new XmlSerializer(typeof(GameModel));
                        return serializer.Deserialize(stream) as GameModel;
                    }
                    catch (System.IO.IOException e)
                    {
                        MessageBox.Show(e.Source + ": " + e.InnerException);
                        return new GameModel();
                    }
                }
            }
            else
                return new GameModel();
        }
    }
}
