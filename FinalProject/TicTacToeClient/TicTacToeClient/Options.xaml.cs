﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TicTacToeClient
{
    /// <summary>
    /// Interaction logic for Options.xaml
    /// </summary>
    public partial class Options : Window
    {
        private OptionsModel _optionsModel;
        public Options()
        {
            _optionsModel = OptionsModel.Create();
            DataContext = _optionsModel;
            InitializeComponent();
        }
    
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            try
            {
                _optionsModel.Save();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Source + ": " + exception.InnerException);
            }
            this.Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            _optionsModel = null;
            Close();
        }
    }
}
