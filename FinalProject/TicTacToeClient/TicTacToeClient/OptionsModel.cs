﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace TicTacToeClient
{
    [Serializable]
    public class OptionsModel : INotifyPropertyChanged
    {
        public OptionsModel()
        {
        }
        //Set Property Changed Event Handler
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        //Set Options RoutedCommands
        public static RoutedCommand OptionsDialogueRoutedCommand = new RoutedCommand("Show Options", typeof(OptionsModel), new InputGestureCollection(new List<InputGesture> { new KeyGesture(Key.O, ModifierKeys.Control) }));

        //Set Computer Player
        private bool _computerPlayer = true;
        public bool ComputerPlayer
        {
            get { return _computerPlayer; }
            set
            {
                _computerPlayer = value;
                OnPropertyChanged(nameof(ComputerPlayer));
            }
        }

        //Set Computer Player Level
        private ComputerPlayLevels _computerPlayLevel = ComputerPlayLevels.Easy;
        public ComputerPlayLevels ComputerPlayLevel
        {
            get { return _computerPlayLevel; }
            set
            {
                _computerPlayLevel = value;
                OnPropertyChanged(nameof(ComputerPlayLevel));
            }
        }

        private string _player1Name = "";
        public string Player1Name {
            get { return _player1Name; }
            set
            {
                _player1Name = value;
                OnPropertyChanged(nameof(Player1Name));
            }
        }
        private string _player2Name = "";
        public string Player2Name
        {
            get { return _player2Name; }
            set
            {
                _player2Name = value;
                OnPropertyChanged(nameof(Player2Name));
            }
        }
        private string _gameService = "http://localhost:22117/GameEngine.svc";
        public string GameService
        {
            get { return _gameService; }
            set
            {
                _gameService = value;
                OnPropertyChanged(nameof(GameService));
            }
        }
        public void Save()
        {
            using (var stream = File.Open("GameOptionsModel.xml", FileMode.Create))
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(OptionsModel));
                    serializer.Serialize(stream, this);
                }
                catch(System.IO.IOException e)
                {                  
                }
                
            }
        }
        public static OptionsModel Create()
        {
            if (File.Exists("GameOptionsModel.xml"))
            {
                using (var stream = File.OpenRead("GameOptionsModel.xml"))
                {
                    try
                    {
                        var serializer = new XmlSerializer(typeof(OptionsModel));
                        return serializer.Deserialize(stream) as OptionsModel;
                    }
                    catch (System.IO.IOException e)
                    {
                        return new OptionsModel();
                    }
                }
            }
            else
                return new OptionsModel();
        }
    }
}
