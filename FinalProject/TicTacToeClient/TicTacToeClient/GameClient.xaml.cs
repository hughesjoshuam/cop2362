﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TicTacToeService;

namespace TicTacToeClient
{
    /// <summary>
    /// Interaction logic for GameClient.xaml
    /// </summary>
    
    public partial class GameClient : Window
    {
        private GameModel _gameModel = new GameModel();
        private OptionsModel _optionsModel = new OptionsModel();
        private bool _winner = false;
        private bool _cat = false;
                
        // Set RoutedCommands
        public static RoutedCommand NewGameRoutedCommand = new RoutedCommand("New Game", typeof(GameModel), new InputGestureCollection(new List<InputGesture> { new KeyGesture(Key.N, ModifierKeys.Control) }));
        public static RoutedCommand AboutDialogueRoutedCommand = new RoutedCommand("About Dialog", typeof(GameModel));

        // Initialize the Game Client
        public GameClient()
        {
            // Instantiate Game Model and check if game started
            if (!_gameModel.GameStarted)
            {
                var dialog = new Options();
                var result = dialog.ShowDialog(); // if game not started show options
            }
            InitializeComponent();
            StartNewGame();
        }

        public void StartNewGame()
        {
            // Reset Winner
            _winner = false;
            _cat = false;

            // Instantiate Game Model and check if game started
            _gameModel = new GameModel();
            DataContext = _gameModel;

            // Reset GameState
            _gameModel.GameState = new string[9];
            _gameModel.GameStarted = true;
            _gameModel.CurrentPlayer = 1;

            // Reset GameClient
            Color colorTransparent = Color.FromRgb(0, 0, 0);
            SolidColorBrush labelBrush = new SolidColorBrush(colorTransparent);
            label0.Content = null;
            label0.IsEnabled = true;
            label0.Foreground = labelBrush;
            label1.Content = null;
            label1.IsEnabled = true;
            label1.Foreground = labelBrush;
            label2.Content = null;
            label2.IsEnabled = true;
            label2.Foreground = labelBrush;
            label3.Content = null;
            label3.IsEnabled = true;
            label3.Foreground = labelBrush;
            label4.Content = null;
            label4.IsEnabled = true;
            label4.Foreground = labelBrush;
            label5.Content = null;
            label5.IsEnabled = true;
            label5.Foreground = labelBrush;
            label6.Content = null;
            label6.IsEnabled = true;
            label6.Foreground = labelBrush;
            label7.Content = null;
            label7.IsEnabled = true;
            label7.Foreground = labelBrush;
            label8.Content = null;
            label8.IsEnabled = true;
            label8.Foreground = labelBrush;
            currentPlayerNameLabelFor.Content = "Current Player:";
            currentPlayerNameLabel.Foreground = labelBrush;

            // Set Game State
            SetGameState();

            //Retrieve Game Options 
            _optionsModel = OptionsModel.Create();
            currentPlayerNameLabel.Content = _optionsModel.Player1Name;

        }
        // Control Game Client Interaction Styles
        private void gameClientMouseEnter(object sender, System.EventArgs e)
        {
            Label gameLabel = (Label)sender;
            Color colorGray = Color.FromRgb(192, 192, 192);
            SolidColorBrush labelBrush = new SolidColorBrush(colorGray);
            gameLabel.Background = labelBrush;
            Mouse.OverrideCursor = Cursors.Hand;
        }
        private void gameClientMouseLeave(object sender, System.EventArgs e)
        {
            Label gameLabel = (Label)sender;
            Color colorTransparent = Color.FromArgb(100, 20, 189, 172);
            SolidColorBrush labelBrush = new SolidColorBrush(colorTransparent);
            gameLabel.Background = labelBrush;
            Mouse.OverrideCursor = null;
        }

        // Control Game Client Commands
        private void CommandCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Close)
                e.CanExecute = true;
            if (e.Command == OptionsModel.OptionsDialogueRoutedCommand)
                e.CanExecute = true;
            if (e.Command == GameClient.AboutDialogueRoutedCommand)
                e.CanExecute = true;
            if (e.Command == GameClient.NewGameRoutedCommand)
                e.CanExecute = true;
            if (e.Command == GameModel.SaveGameRoutedCommand)
            {
                if (_gameModel.GameStarted == true)
                {
                    e.CanExecute = true;
                }
                else
                {
                    e.CanExecute = false;
                }
            }
            if (e.Command == GameModel.OpenGameRoutedCommand)
                e.CanExecute = true;
            e.Handled = true;
        }
        public void CommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Close)
                this.Close();
            if (e.Command == OptionsModel.OptionsDialogueRoutedCommand)
            {
                var dialog = new Options();
                var result = dialog.ShowDialog();
                // Options have changed -> reset game
                if (result.HasValue && result.Value == true)
                {
                    StartNewGame();
                }
            }
            if (e.Command == GameClient.AboutDialogueRoutedCommand)
            {
                var dialog = new About();
                dialog.ShowDialog();
            }
            if (e.Command == GameClient.NewGameRoutedCommand)
            {
                StartNewGame();
            }
            if (e.Command == GameModel.SaveGameRoutedCommand)
            {
                _gameModel.Save();
                _optionsModel.Save();
            }
            if (e.Command == GameModel.OpenGameRoutedCommand)
            {
                StartNewGame();
                GameModel _gameModel = GameModel.Create();
                OptionsModel _optionsModel = OptionsModel.Create();
                label0.Content = _gameModel.GameState[0];
                label1.Content = _gameModel.GameState[1];
                label2.Content = _gameModel.GameState[2];
                label3.Content = _gameModel.GameState[3];
                label4.Content = _gameModel.GameState[4];
                label5.Content = _gameModel.GameState[5];
                label6.Content = _gameModel.GameState[6];
                label7.Content = _gameModel.GameState[7];
                label8.Content = _gameModel.GameState[8];
                if (_gameModel.CurrentPlayer == 1)
                {
                    _gameModel.CurrentPlayer = 1;
                    currentPlayerNameLabel.Content = _optionsModel.Player1Name;
                }
                else
                {
                    if(_optionsModel.ComputerPlayer == true)
                    {
                        _gameModel.CurrentPlayer = 1;
                        currentPlayerNameLabel.Content = "Computer";
                        // Retrieve the Game State Value for Computer Player
                        if (_optionsModel.ComputerPlayer == true)
                            GetGameState();
                    } else
                    {
                        _gameModel.CurrentPlayer = 2;
                        currentPlayerNameLabel.Content = _optionsModel.Player2Name;
                    }
                }
            }
            e.Handled = true;
        }

        // GameEngine Interaction Methods
        private void GetGameState()
        {


            string gameState = GameEngine();
            

            //Compare GameModel with GameState to identify computer play
            for (int i = 0; i <=8; i++)
            {
                if (gameState[i].ToString().Equals("1"))
                {
                    switch (i)
                    {
                        case 0:
                            label0.Content = "O";
                            label0.IsEnabled = false;
                            break;
                        case 1:
                            label1.Content = "O";
                            label1.IsEnabled = false;
                            break;
                        case 2:
                            label2.Content = "O";
                            label2.IsEnabled = false;
                            break;
                        case 3:
                            label3.Content = "O";
                            label3.IsEnabled = false;
                            break;
                        case 4:
                            label4.Content = "O";
                            label4.IsEnabled = false;
                            break;
                        case 5:
                            label5.Content = "O";
                            label5.IsEnabled = false;
                            break;
                        case 6:
                            label6.Content = "O";
                            label6.IsEnabled = false;
                            break;
                        case 7:
                            label7.Content = "O";
                            label7.IsEnabled = false;
                            break;
                        case 8:
                            label8.Content = "O";
                            label8.IsEnabled = false;
                            break;
                    }
                }
            }

            // Set Game State
            SetGameState();

            // Check for Winner
            WinnerCheck();

            // Update GameEngine with WinnerStatus
            if(_winner)
                GameEngine();

        }
        private void SetGameState()
        {
            // Set Game State in Client
            if (label0.Content != null)
                _gameModel.GameState[0] = label0.Content.ToString();
            if (label1.Content != null)
                _gameModel.GameState[1] = label1.Content.ToString();
            if (label2.Content != null)
                _gameModel.GameState[2] = label2.Content.ToString();
            if (label3.Content != null)
                _gameModel.GameState[3] = label3.Content.ToString();
            if (label4.Content != null)
                _gameModel.GameState[4] = label4.Content.ToString();
            if (label5.Content != null)
                _gameModel.GameState[5] = label5.Content.ToString();
            if (label6.Content != null)
                _gameModel.GameState[6] = label6.Content.ToString();
            if (label7.Content != null)
                _gameModel.GameState[7] = label7.Content.ToString();
            if (label8.Content != null)
                _gameModel.GameState[8] = label8.Content.ToString();
        }

        // Game Play Actions
        public void gamePlayAction(object sender, System.EventArgs e)
        {
            

            // Instantiate the Game Label
            Label gameLabel = (Label)sender;

            // Set Game Label Value
            if(_gameModel.CurrentPlayer == 1)
            {
                gameLabel.Content = "X";
            }else
            {
                gameLabel.Content = "O";
            }

            // Disable Game Label from further plays
            gameLabel.IsEnabled = false;
            
            // Set the Game State Value
            SetGameState();

            // Check for Winner
            WinnerCheck();

            // Update GameEngine with WinnerStatus
            if (_winner)
                GameEngine();

            // Retrieve Get Game State Value for Computer Player
            if (_optionsModel.ComputerPlayer == true && !_winner)
                GetGameState();
        }
        public void WinnerCheck()
        {
            // Check for Winner
            Color colorTransparent = Color.FromRgb(239, 255, 66);
            SolidColorBrush labelBrush = new SolidColorBrush(colorTransparent);
            if (_gameModel.GameState[0] != "" && _gameModel.GameState[0] != null &&
                _gameModel.GameState[1] != "" && _gameModel.GameState[1] != null &&
                _gameModel.GameState[2] != "" && _gameModel.GameState[2] != null &&
                _gameModel.GameState[0] == _gameModel.GameState[1]
                && _gameModel.GameState[1] == _gameModel.GameState[2])
            {
                _winner = true;
                label0.Foreground = labelBrush;
                label1.Foreground = labelBrush;
                label2.Foreground = labelBrush;
            }
            else if (_gameModel.GameState[0] != "" && _gameModel.GameState[0] != null &&
                _gameModel.GameState[4] != "" && _gameModel.GameState[4] != null &&
                _gameModel.GameState[8] != "" && _gameModel.GameState[8] != null &&
                _gameModel.GameState[0] == _gameModel.GameState[4]
                && _gameModel.GameState[4] == _gameModel.GameState[8])
            {
                _winner = true;
                label0.Foreground = labelBrush;
                label4.Foreground = labelBrush;
                label8.Foreground = labelBrush;
            }
            else if (_gameModel.GameState[0] != "" && _gameModel.GameState[0] != null &&
                _gameModel.GameState[3] != "" && _gameModel.GameState[3] != null &&
                _gameModel.GameState[6] != "" && _gameModel.GameState[6] != null &&
                _gameModel.GameState[0] == _gameModel.GameState[3] &&
                _gameModel.GameState[3] == _gameModel.GameState[6])
            {
                _winner = true;
                label0.Foreground = labelBrush;
                label3.Foreground = labelBrush;
                label6.Foreground = labelBrush;
            }
            else if (_gameModel.GameState[3] != "" && _gameModel.GameState[3] != null &&
                _gameModel.GameState[4] != "" && _gameModel.GameState[4] != null &&
                _gameModel.GameState[5] != "" && _gameModel.GameState[5] != null &&
                _gameModel.GameState[3] == _gameModel.GameState[4] &&
                _gameModel.GameState[4] == _gameModel.GameState[5])
            {
                _winner = true;
                label3.Foreground = labelBrush;
                label4.Foreground = labelBrush;
                label5.Foreground = labelBrush;
            }
            else if (_gameModel.GameState[6] != "" && _gameModel.GameState[6] != null &&
                _gameModel.GameState[4] != "" && _gameModel.GameState[4] != null &&
                _gameModel.GameState[2] != "" && _gameModel.GameState[2] != null &&
                _gameModel.GameState[6] == _gameModel.GameState[4] &&
                _gameModel.GameState[4] == _gameModel.GameState[2])
            {
                _winner = true;
                label6.Foreground = labelBrush;
                label4.Foreground = labelBrush;
                label2.Foreground = labelBrush;
            }
            else if (_gameModel.GameState[6] != "" && _gameModel.GameState[6] != null &&
                _gameModel.GameState[7] != "" && _gameModel.GameState[7] != null &&
                _gameModel.GameState[8] != "" && _gameModel.GameState[8] != null &&
                _gameModel.GameState[6] == _gameModel.GameState[7] &&
                _gameModel.GameState[7] == _gameModel.GameState[8])
            {
                _winner = true;
                label6.Foreground = labelBrush;
                label7.Foreground = labelBrush;
                label8.Foreground = labelBrush;
            }
            else if (_gameModel.GameState[1] != "" && _gameModel.GameState[1] != null &&
                _gameModel.GameState[4] != "" && _gameModel.GameState[4] != null &&
                _gameModel.GameState[7] != "" && _gameModel.GameState[7] != null &&
                _gameModel.GameState[1] == _gameModel.GameState[4] &&
                _gameModel.GameState[4] == _gameModel.GameState[7])
            {
                _winner = true;
                label1.Foreground = labelBrush;
                label4.Foreground = labelBrush;
                label7.Foreground = labelBrush;
            }
            else if (_gameModel.GameState[2] != "" && _gameModel.GameState[2] != null &&
                _gameModel.GameState[5] != "" && _gameModel.GameState[5] != null &&
                _gameModel.GameState[8] != "" && _gameModel.GameState[8] != null &&
                _gameModel.GameState[2] == _gameModel.GameState[5] &&
                _gameModel.GameState[5] == _gameModel.GameState[8])
            {
                _winner = true;
                label2.Foreground = labelBrush;
                label5.Foreground = labelBrush;
                label8.Foreground = labelBrush;
            }
            else
            {
                // Check for cat winner
                if (_gameModel.GameState[0] != "" && _gameModel.GameState[0] != null &&
                    _gameModel.GameState[1] != "" && _gameModel.GameState[1] != null &&
                    _gameModel.GameState[2] != "" && _gameModel.GameState[2] != null &&
                    _gameModel.GameState[3] != "" && _gameModel.GameState[3] != null &&
                    _gameModel.GameState[4] != "" && _gameModel.GameState[4] != null &&
                    _gameModel.GameState[5] != "" && _gameModel.GameState[5] != null &&
                    _gameModel.GameState[6] != "" && _gameModel.GameState[6] != null &&
                    _gameModel.GameState[7] != "" && _gameModel.GameState[7] != null &&
                    _gameModel.GameState[8] != "" && _gameModel.GameState[8] != null)
                {
                    _winner = true;
                    _cat = true;
                }
                    
            }
            if (_winner)
            {
                label0.IsEnabled = false;
                label1.IsEnabled = false;
                label2.IsEnabled = false;
                label3.IsEnabled = false;
                label4.IsEnabled = false;
                label5.IsEnabled = false;
                label6.IsEnabled = false;
                label7.IsEnabled = false;
                label8.IsEnabled = false;
                currentPlayerNameLabelFor.Content = "WINNER:";
                if (_cat)
                    currentPlayerNameLabel.Content = "CAT";
                currentPlayerNameLabel.Foreground = labelBrush;
                
            }
            else
            {
                // Nobody wone so set next player 
                if (_gameModel.CurrentPlayer == 1)
                {
                    _gameModel.CurrentPlayer = 2;
                    if (_optionsModel.ComputerPlayer == true)
                    {
                        currentPlayerNameLabel.Content = "Computer";
                    }
                    else
                    {
                        currentPlayerNameLabel.Content = _optionsModel.Player2Name.ToString();
                    }
                }
                else
                {
                    _gameModel.CurrentPlayer = 1;
                    currentPlayerNameLabel.Content = _optionsModel.Player1Name.ToString();
                }
            }

        }
        public String GameEngine()
        {
            /*
             * Game Engine Definition
             * GetGameState(int gameState);
             * Value Mask Bit Assignment: null = 0, assigned = 1
             * Value Mask Assignment: ######### (e.g. 000001001, 100100111)
             * Value Bit Assignment: Null = 0, X = 0, O = 1           
             * Fields: ######### (e.g. 001011010)
             * Computer Play Level Bit Assignment: Easy = 00, Medium = 01, Difficult = 10, Learning = 11
             * Computer Play Level: ## + Fields (e.g. 00001011010, 01001011010, 10001011010, 11001011010)
             */

            // Prepare data for submission to Game Engine
            string playLevel = "00", valueMask = "000000000", fieldValues = "000000000", gameState = "000000000";

            // Set Computer Play Level
            switch (_optionsModel.ComputerPlayLevel)
            {
                case ComputerPlayLevels.Easy:
                    playLevel = "00";
                    break;
                case ComputerPlayLevels.Medium:
                    playLevel = "01";
                    break;
                case ComputerPlayLevels.Difficult:
                    playLevel = "10";
                    break;
                case ComputerPlayLevels.Learning:
                    playLevel = "11";
                    break;
                default:
                    playLevel = "11";
                    break;
            }
            // Set value mask and field value
            char[] aryValueMask = valueMask.ToArray();
            char[] aryFieldValues = fieldValues.ToArray();

            for (int i = 0; i <= 8; i++)
            {
                if (_gameModel.GameState[i] != null)
                {
                    if (!_gameModel.GameState[i].ToString().Equals(""))
                    {
                        if (_gameModel.GameState[i].ToString().Equals("X"))
                        {
                            aryValueMask[i] = Convert.ToChar("1");
                        }
                        else
                        {
                            aryValueMask[i] = Convert.ToChar("1");
                            aryFieldValues[i] = Convert.ToChar("1");
                        }
                    }
                }
            }
            valueMask = new string(aryValueMask);
            fieldValues = new string(aryFieldValues);

            // Sets and Retrieves game state from WCF service if Computer Player is true

            // Instantiate ChannelFactory Client
            ChannelFactory<IGameEngine> GameEngine = new ChannelFactory<IGameEngine>();

            // Set client EndPointAddress
            /* 
             * Local WCF Service: http://localhost:22117/GameEngine.svc
            */

            EndpointAddress endPointAddress = new EndpointAddress(_optionsModel.GameService);

            GameEngine.Endpoint.Address = endPointAddress;

            // Set client Binding
            System.ServiceModel.Channels.Binding binding = new WSHttpBinding();
            GameEngine.Endpoint.Binding = binding;

            //Set ClientCredentials for Windows Authentication
            var gameEngineCredentials = GameEngine.Credentials;
            gameEngineCredentials.UserName.UserName = "hughesjm80@live.com";
            gameEngineCredentials.UserName.Password = "";

            // Establish connection to Game Engine
            IGameEngine GameEngineProxy = GameEngine.CreateChannel();

            // Set playLevel with Game Engine
            try
            {
                GameEngineProxy.PlayLevel(playLevel);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Source + ": " + exception.Message, "GameEngine-Set Play Level");
            }
            // Set valueMask with Game Engine
            try
            {
                GameEngineProxy.ValueMask(valueMask);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Source + ": " + exception.Message, "GameEngine-Set Value Mask");
            }
            // Set fieldValues with Game Engine
            try
            {
                GameEngineProxy.FieldValues(fieldValues);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Source + ": " + exception.Message, "GameEngine-Set Field Values");
            }
            // Retrieve new gameState from Game Engine if Winner is false
            try
            {
                if (!_winner)
                    gameState = GameEngineProxy.GameState();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Source + ": " + exception.Message, "GameEngine-Get Game State");
            }
            // Report Win, Loss, or Continue to GameEngine
            if (_winner)
            {
                if (_cat)
                {
                    try
                    {
                        GameEngineProxy.WinnerStatus(3);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Source + ": " + exception.Message, "GameEngine-Set Winner");
                    }
                }
                else
                {
                    try
                    {
                        GameEngineProxy.WinnerStatus(_gameModel.CurrentPlayer);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Source + ": " + exception.Message, "GameEngine-Set Winner");
                    }
                }
            }
            else
            {
                try
                {
                    GameEngineProxy.WinnerStatus(null);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Source + ": " + exception.Message, "GameEngine-Set Winner");
                }
            }

            // Close GameEngine Connection
            GameEngine.Close();
            return gameState;
        }
    }
}
