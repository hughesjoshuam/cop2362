﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TicTacToeService
{
    public class GameEngine : IGameEngine
    {
        private string _playLevel;
        private string _valueMask;
        private string _fieldValues;
        private string _gameState;
        bool learning = false;
        bool gameStateExists = false;
        private GameEngineModel _gameEngineModel = GameEngineModel.Create();

        public void PlayLevel(string playLevel)
        {
            _playLevel = playLevel;
        }
        public void ValueMask(string valueMask)
        {
            _valueMask = valueMask;
        }
        public void FieldValues(string fieldValues)
        {
            _fieldValues = fieldValues;
        }
        public string GameState()
        {
            playAction();
            return _gameState;
        }
        public void WinnerStatus(int? winnerStatus)
        {
            // If _winnerStatus is true Save Game State
            if (winnerStatus != null)
            {
                // Check that GameStates is not null
                if (_gameEngineModel.GameStates != null)
                {
                    // Loop through GameStates to determine if GameState exists
                    for (int i = 0; i < _gameEngineModel.GameStates.Count(); i++)
                    {
                        // Retrieve winner, gameStateValue, and Priority
                        string gameStateValue = _gameEngineModel.GameStates[i].Substring(0, 9);
                        int gameStatePriority = int.Parse(_gameEngineModel.GameStates[i].Substring(9));

                        if (gameStateValue.Equals(_fieldValues))
                        {
                            // Set GameStateExists to true becase it was located
                            gameStateExists = true;

                            if (winnerStatus == 2)
                            {
                                // If GameState Exists and Winner Status is computer increase priority
                                gameStatePriority += 5;
                            }
                            else if (winnerStatus == 3)
                            {
                                // If GameState Exists and Winner Status is CAT increase priority slightly
                                gameStatePriority += 2;
                                if (gameStatePriority < 0)
                                    gameStatePriority = 0;
                            }else if(winnerStatus == 2)
                            {
                                // If GameState Exists and Winner Status is player 1 decrease priority
                                gameStatePriority -= 5;
                                if (gameStatePriority < 0)
                                    gameStatePriority = 0;
                            }

                            // Concatenate GameStateValue and new GameStatePriority
                            gameStateValue = gameStateValue + gameStatePriority.ToString();

                            // Update GameStateValue and GameStatePriority in GameStates
                            _gameEngineModel.GameStates[i] = gameStateValue;
                        }
                    }
                }
                // If GameStateExists was false add new GameState to GameStatesModel
                if (!gameStateExists)
                {
                    // Concatenate GameStateValue and new GameStatePriority
                    string gameStateValue = _fieldValues + "0";
                    if (_gameEngineModel.GameStates != null)
                    {
                        List<string> _gameStates = _gameEngineModel.GameStates.ToList();
                        _gameStates.Insert(0, gameStateValue);
                        _gameEngineModel.GameStates = _gameStates.ToArray();
                    } else
                    {
                        _gameEngineModel.GameStates = new string[] { gameStateValue };
                    }
                }

                // Save GameStatesModel
                _gameEngineModel.Save();
            }
        }

        private void playAction()
        {
            // Set variable defaults
            Random random = new Random();
            int randomNumber = random.Next(0, 9);
            bool loop = true;
            bool corner = true;
            bool center = true;

            char[] _valueMaskArray = _valueMask.ToArray();
            char[] _fieldValuesArray = _fieldValues.ToArray();

            if (_playLevel.Equals("00"))
            {
                // EASY
                while (loop)
                {
                    if (_valueMaskArray[randomNumber].ToString().Equals("0"))
                    {
                        _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                        loop = false;
                        break;
                    } else
                    {
                        randomNumber = random.Next(0, 9);
                    }
                }
                loop = true;

            } else if (_playLevel.Equals("01"))
            {
                // MEDIUM
                // Check for three or more Corners
                if ((_valueMaskArray[0].ToString().Equals("1") && 
                    _valueMaskArray[2].ToString().Equals("1") &&
                    _valueMaskArray[8].ToString().Equals("1")) ||
                    (_valueMaskArray[2].ToString().Equals("1") &&
                    _valueMaskArray[8].ToString().Equals("1") &&
                    _valueMaskArray[6].ToString().Equals("1")) ||
                    (_valueMaskArray[8].ToString().Equals("1") &&
                    _valueMaskArray[0].ToString().Equals("1") &&
                    _valueMaskArray[6].ToString().Equals("1")) ||
                    (_valueMaskArray[0].ToString().Equals("1") &&
                    _valueMaskArray[2].ToString().Equals("1") &&
                    _valueMaskArray[6].ToString().Equals("1")))
                    corner = false;                  

                // Check for center
                if (_valueMaskArray[4].ToString().Equals("1"))
                    center = false;

                // Random Corner First
                if (corner)
                {
                    while (loop)
                    {
                        if ((randomNumber == 0 || randomNumber == 2 || randomNumber == 6 || randomNumber == 8) &&
                            _valueMaskArray[randomNumber].ToString().Equals("0"))
                        {
                            _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                            loop = false;
                            break;
                        }
                        else
                        {
                            randomNumber = random.Next(0, 9);
                        }
                    }
                    loop = true;
                }
                // Then Center
                if (!corner && center)
                    _fieldValuesArray[4] = Convert.ToChar("1");
                // Then Random Selection
                if (!corner && !center)
                {
                    while (loop)
                    {
                        if (_valueMaskArray[randomNumber].ToString().Equals("0"))
                        {
                            _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                            loop = false;
                            break;
                        }
                        else
                        {
                            randomNumber = random.Next(0, 9);
                        }
                    }
                    loop = true;
                }

            }
            else if (_playLevel.Equals("10"))
            {
                // DIFFICULT

                // Check for two in a row with O and WIN
                if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[1].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("1") && _fieldValuesArray[1].ToString().Equals("1")))
                {
                    _fieldValuesArray[2] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[1].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("1") && _fieldValuesArray[2].ToString().Equals("1")))
                {
                    _fieldValuesArray[1] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[3].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("1") && _fieldValuesArray[6].ToString().Equals("1")))
                {
                    _fieldValuesArray[3] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[1].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[0].ToString().Equals("0")) &&
                    (_fieldValuesArray[1].ToString().Equals("1") && _fieldValuesArray[2].ToString().Equals("1")))
                {
                    _fieldValuesArray[0] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[3].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("0")) &&
                    (_fieldValuesArray[3].ToString().Equals("1") && _fieldValuesArray[4].ToString().Equals("1")))
                {
                    _fieldValuesArray[5] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[3].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("0")) &&
                    (_fieldValuesArray[3].ToString().Equals("1") && _fieldValuesArray[5].ToString().Equals("1")))
                {
                    _fieldValuesArray[4] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[3].ToString().Equals("0")) &&
                    (_fieldValuesArray[4].ToString().Equals("1") && _fieldValuesArray[5].ToString().Equals("1")))
                {
                    _fieldValuesArray[3] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[7].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("0")) &&
                    (_fieldValuesArray[6].ToString().Equals("1") && _fieldValuesArray[7].ToString().Equals("1")))
                {
                    _fieldValuesArray[8] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[7].ToString().Equals("0")) &&
                    (_fieldValuesArray[6].ToString().Equals("1") && _fieldValuesArray[8].ToString().Equals("1")))
                {
                    _fieldValuesArray[7] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[7].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("0")) &&
                    (_fieldValuesArray[7].ToString().Equals("1") && _fieldValuesArray[8].ToString().Equals("1")))
                {
                    _fieldValuesArray[6] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("1") && _fieldValuesArray[4].ToString().Equals("1")))
                {
                    _fieldValuesArray[8] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[0].ToString().Equals("0")) &&
                    (_fieldValuesArray[4].ToString().Equals("1") && _fieldValuesArray[8].ToString().Equals("1")))
                {
                    _fieldValuesArray[0] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("1") && _fieldValuesArray[1].ToString().Equals("1")))
                {
                    _fieldValuesArray[4] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("0")) &&
                    (_fieldValuesArray[2].ToString().Equals("1") && _fieldValuesArray[4].ToString().Equals("1")))
                {
                    _fieldValuesArray[6] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("1") && _fieldValuesArray[1].ToString().Equals("1")))
                {
                    _fieldValuesArray[2] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("0")) &&
                    (_fieldValuesArray[2].ToString().Equals("1") && _fieldValuesArray[6].ToString().Equals("1")))
                {
                    _fieldValuesArray[4] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[1].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("1") && _fieldValuesArray[2].ToString().Equals("1")))
                {
                    _fieldValuesArray[1] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("0")) &&
                    (_fieldValuesArray[2].ToString().Equals("1") && _fieldValuesArray[8].ToString().Equals("1")))
                {
                    _fieldValuesArray[5] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("0")) &&
                    (_fieldValuesArray[2].ToString().Equals("1") && _fieldValuesArray[5].ToString().Equals("1")))
                {
                    _fieldValuesArray[8] = Convert.ToChar("1");
                }
                else if ((_valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("0")) &&
                    (_fieldValuesArray[8].ToString().Equals("1") && _fieldValuesArray[5].ToString().Equals("1")))
                {
                    _fieldValuesArray[2] = Convert.ToChar("1");
                }
                else
                {
                    // Check for two in a row with X then block
                    if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[1].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("0")) &&
                    (_fieldValuesArray[0].ToString().Equals("0") && _fieldValuesArray[1].ToString().Equals("0")))
                    {
                        _fieldValuesArray[2] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[1].ToString().Equals("0")) &&
                        (_fieldValuesArray[0].ToString().Equals("0") && _fieldValuesArray[2].ToString().Equals("0")))
                    {
                        _fieldValuesArray[1] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[3].ToString().Equals("0")) &&
                        (_fieldValuesArray[0].ToString().Equals("0") && _fieldValuesArray[6].ToString().Equals("0")))
                    {
                        _fieldValuesArray[3] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[1].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[0].ToString().Equals("0")) &&
                        (_fieldValuesArray[1].ToString().Equals("0") && _fieldValuesArray[2].ToString().Equals("0")))
                    {
                        _fieldValuesArray[0] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[3].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("0")) &&
                        (_fieldValuesArray[3].ToString().Equals("0") && _fieldValuesArray[4].ToString().Equals("0")))
                    {
                        _fieldValuesArray[5] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[3].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("0")) &&
                        (_fieldValuesArray[3].ToString().Equals("0") && _fieldValuesArray[5].ToString().Equals("0")))
                    {
                        _fieldValuesArray[4] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[3].ToString().Equals("0")) &&
                        (_fieldValuesArray[4].ToString().Equals("0") && _fieldValuesArray[5].ToString().Equals("0")))
                    {
                        _fieldValuesArray[3] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[7].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("0")) &&
                        (_fieldValuesArray[6].ToString().Equals("0") && _fieldValuesArray[7].ToString().Equals("0")))
                    {
                        _fieldValuesArray[8] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[7].ToString().Equals("0")) &&
                        (_fieldValuesArray[6].ToString().Equals("0") && _fieldValuesArray[8].ToString().Equals("0")))
                    {
                        _fieldValuesArray[7] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[7].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("0")) &&
                        (_fieldValuesArray[7].ToString().Equals("0") && _fieldValuesArray[8].ToString().Equals("0")))
                    {
                        _fieldValuesArray[6] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("0")) &&
                        (_fieldValuesArray[0].ToString().Equals("0") && _fieldValuesArray[4].ToString().Equals("0")))
                    {
                        _fieldValuesArray[8] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[0].ToString().Equals("0")) &&
                        (_fieldValuesArray[4].ToString().Equals("0") && _fieldValuesArray[8].ToString().Equals("0")))
                    {
                        _fieldValuesArray[0] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("0")) &&
                        (_fieldValuesArray[0].ToString().Equals("0") && _fieldValuesArray[1].ToString().Equals("0")))
                    {
                        _fieldValuesArray[4] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("0")) &&
                        (_fieldValuesArray[2].ToString().Equals("0") && _fieldValuesArray[4].ToString().Equals("0")))
                    {
                        _fieldValuesArray[6] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[4].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("0")) &&
                        (_fieldValuesArray[0].ToString().Equals("0") && _fieldValuesArray[1].ToString().Equals("0")))
                    {
                        _fieldValuesArray[2] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") && _valueMaskArray[4].ToString().Equals("0")) &&
                        (_fieldValuesArray[2].ToString().Equals("0") && _fieldValuesArray[6].ToString().Equals("0")))
                    {
                        _fieldValuesArray[4] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[1].ToString().Equals("0")) &&
                        (_fieldValuesArray[0].ToString().Equals("0") && _fieldValuesArray[2].ToString().Equals("0")))
                    {
                        _fieldValuesArray[1] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("0")) &&
                        (_fieldValuesArray[2].ToString().Equals("0") && _fieldValuesArray[8].ToString().Equals("0")))
                    {
                        _fieldValuesArray[5] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[8].ToString().Equals("0")) &&
                        (_fieldValuesArray[2].ToString().Equals("0") && _fieldValuesArray[5].ToString().Equals("0")))
                    {
                        _fieldValuesArray[8] = Convert.ToChar("1");
                    }
                    else if ((_valueMaskArray[8].ToString().Equals("1") && _valueMaskArray[5].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("0")) &&
                        (_fieldValuesArray[8].ToString().Equals("0") && _fieldValuesArray[5].ToString().Equals("0")))
                    {
                        _fieldValuesArray[2] = Convert.ToChar("1");
                    }
                    else
                    {
                        // Check for three corners and take the fourth
                        if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") &&
                            _valueMaskArray[6].ToString().Equals("1")) && _valueMaskArray[8].ToString().Equals("0"))
                        {
                            _fieldValuesArray[8] = Convert.ToChar("1");
                        }
                        else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[2].ToString().Equals("1") &&
                            _valueMaskArray[8].ToString().Equals("1")) && _valueMaskArray[6].ToString().Equals("0"))
                        {
                            _fieldValuesArray[6] = Convert.ToChar("1");
                        }
                        else if ((_valueMaskArray[0].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") &&
                            _valueMaskArray[8].ToString().Equals("1")) && _valueMaskArray[2].ToString().Equals("0"))
                        {
                            _fieldValuesArray[2] = Convert.ToChar("1");
                        }
                        else if ((_valueMaskArray[2].ToString().Equals("1") && _valueMaskArray[6].ToString().Equals("1") &&
                            _valueMaskArray[8].ToString().Equals("1")) && _valueMaskArray[0].ToString().Equals("0"))
                        {
                            _fieldValuesArray[0] = Convert.ToChar("1");
                        }
                        else
                        {
                            // Random Selection
                            while (loop)
                            {
                                if (_valueMaskArray[randomNumber].ToString().Equals("0"))
                                {
                                    _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                                    loop = false;
                                    break;
                                }
                                else
                                {
                                    randomNumber = random.Next(0, 9);
                                }
                            }
                            loop = true;
                        }
                    }
                }
            } else if (_playLevel.Equals("11"))
            {
                // LEARNING

                string _gamePlayFields = null;
                char[] _gamePlayFieldsArray = new char[] { };
                // Check if GameModel has more than 3 Saved GameStates
                if (_gameEngineModel.GameStates != null)
                {
                    if(_gameEngineModel.GameStates.Count() > 3)
                    {
                        learning = true;
                    }
                }

                if (learning)
                {
                    /*
                        If FieldValues is priority
			                Set GameState[Next Priority Move];
			                Save GameState;
		                End if;
                    */
                    // Identify played fields
                    string playedFields = null;
                    for (int i = 0; i < _valueMaskArray.Count(); i++)
                    {
                        if (_valueMaskArray[i].ToString().Equals("1"))
                        {
                            playedFields = playedFields + i.ToString();
                        }
                    }

                    // Game State Comparison
                    string _gameStateComparison = null;
                    foreach (char playedField in playedFields)
                    {
                        _gameStateComparison = _gameStateComparison + _fieldValuesArray[int.Parse(playedField.ToString())].ToString();
                    }

                    //Populate array from GameState into LogicModel and compare played data values
                    IDictionary<int, Dictionary<int, string>> logicModel = new Dictionary<int, Dictionary<int, string>>();
                    for (int i = 0; i < _gameEngineModel.GameStates.Count(); i++)
                    {
                        // Saved Game State Priority
                        int _savedGameStatePriority = int.Parse(_gameEngineModel.GameStates[i].ToString().Substring(9));

                        // Saved Game State Comparison
                        string _savedGameStateComparison = null;
                        foreach (char playedField in playedFields)
                        {
                            _savedGameStateComparison = _savedGameStateComparison + _gameEngineModel.GameStates[i].ToString().Substring(int.Parse(playedField.ToString()), 1);
                        }

                        // Compare Saved Game State to Current Game State if Equal Save Game State and Priority
                        if (_savedGameStateComparison.ToString().Equals(_gameStateComparison.ToString())){
                            // Build Dictionary of Game States that are Equal
                            logicModel.Add(logicModel.Count(), new Dictionary<int, string>() { { _savedGameStatePriority, _gameEngineModel.GameStates[i].ToString().Substring(0,9) } });
                        }
                    }

                    if (logicModel.Count() > 0)
                    {
                        // Identify highest priority game state
                        int _highestPriority = 0;
                        foreach (int key in logicModel.Keys.ToList())
                        {
                            foreach (int priority in logicModel[key].Keys.ToList())
                            {
                                if (priority > _highestPriority)
                                    _highestPriority = priority;
                            }
                        }

                        // If Highest priority greater than zero
                        if (_highestPriority > 0)
                        {
                            // Remove all GameStates that are not highest priority
                            foreach (int key in logicModel.Keys.ToList())
                            {
                                foreach (int priority in logicModel[key].Keys.ToList())
                                {
                                    if (priority != _highestPriority)
                                        logicModel.Remove(key);
                                }
                            }
                            // Determine number of possible GameStates remaining
                            if (logicModel.Count() > 1)
                            {
                                // Determine GameState that won in fewer moves
                                int numberOfMoves = 0; // Starts with no moves and works up
                                int fewerMoves = 9; // Starts with 9 possible and works down
                                int gamePlayKey = 0;
                                int gamePlayPriority = 0;
                                foreach(int key in logicModel.Keys.ToList())
                                {
                                    foreach(int priority in logicModel[key].Keys.ToList())
                                    {
                                        _gamePlayFields = logicModel[key][priority].ToString();
                                        _gamePlayFieldsArray = _gamePlayFields.ToArray();
                                        for(int i = 0; i < _gamePlayFieldsArray.Count(); i++)
                                        {
                                            if (_gamePlayFieldsArray[i].ToString().Equals("1"))
                                            {
                                                numberOfMoves += 1;
                                            }
                                        }
                                        if (numberOfMoves < fewerMoves)
                                        {
                                            fewerMoves = numberOfMoves;
                                            gamePlayKey = key;
                                            gamePlayPriority = priority;
                                        }
                                    }
                                }
                                // Use game state to determine play
                                string _gamePlay = logicModel[gamePlayKey][gamePlayPriority].ToString();
                                char[] _gamePlayArray = _gamePlay.ToArray();
                                // Determine fields with O played
                                for (int i = 0; i < _gamePlayArray.Count(); i++)
                                {
                                    if (_gamePlayArray.ToString().Equals("1"))
                                    {
                                        _gamePlayFields = _gamePlayFields + i.ToString();
                                    }
                                }
                                // Use GamePlay Fields and Random Select Next Play from Possible Fields
                                _gamePlayFieldsArray = _gamePlayFields.ToArray();
                                while (loop)
                                {
                                    if (_gamePlayFieldsArray.Contains(Convert.ToChar(randomNumber.ToString())) &&
                                        _valueMaskArray[randomNumber].ToString().Equals("0"))
                                    {
                                        _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                                        loop = false;
                                        break;
                                    }
                                    else
                                    {
                                        randomNumber = random.Next(0, 9);
                                    }
                                }
                                loop = true;
                            }
                            else
                            {
                                // Use game state to determine play
                                foreach (int key in logicModel.Keys.ToList())
                                {
                                    foreach (int priority in logicModel[key].Keys.ToList())
                                    {
                                        string _gamePlay = logicModel[key][priority].ToString();
                                        char[] _gamePlayArray = _gamePlay.ToArray();
                                        // Determine fields with O played
                                        for (int i = 0; i < _gamePlayArray.Count(); i++)
                                        {
                                            if (_gamePlayArray[i].ToString().Equals("1"))
                                            {
                                                _gamePlayFields = _gamePlayFields + i.ToString();
                                            }
                                        }
                                    }
                                }
                                // Use GamePlay Fields and Random Select Next Play from Possible Fields
                                _gamePlayFieldsArray = _gamePlayFields.ToArray();
                                while (loop)
                                {
                                    if (_gamePlayFieldsArray.Contains(Convert.ToChar(randomNumber.ToString())) &&
                                        _valueMaskArray[randomNumber].ToString().Equals("0"))
                                    {
                                        _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                                        loop = false;
                                        break;
                                    }
                                    else
                                    {
                                        randomNumber = random.Next(0, 9);
                                    }
                                }
                                loop = true;
                            }
                        }
                        else
                        {
                            // No valuable information exists -> Random Play
                            while (loop)
                            {
                                if (_valueMaskArray[randomNumber].ToString().Equals("0"))
                                {
                                    _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                                    loop = false;
                                    break;
                                }
                                else
                                {
                                    randomNumber = random.Next(0, 9);
                                }
                            }
                            loop = true;
                        }
                    }
                    else
                    {
                        // No valuable information exists -> Random Play
                        while (loop)
                        {
                            if (_valueMaskArray[randomNumber].ToString().Equals("0"))
                            {
                                _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                                loop = false;
                                break;
                            }
                            else
                            {
                                randomNumber = random.Next(0, 9);
                            }
                        }
                        loop = true;
                    }
                } else
                {
                    // No valuable information exists -> Random Play
                    while (loop)
                    {
                        if (_valueMaskArray[randomNumber].ToString().Equals("0"))
                        {
                            _fieldValuesArray[randomNumber] = Convert.ToChar("1");
                            loop = false;
                            break;
                        }
                        else
                        {
                            randomNumber = random.Next(0, 9);
                        }
                    }
                    loop = true;
                }
            }
            _gameState = new string(_fieldValuesArray);
        }
    }
}
