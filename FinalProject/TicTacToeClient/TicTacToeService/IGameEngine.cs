﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TicTacToeService
{
    /*
    * Game Engine Definition
    * GetGameState(int gameState);
    * Value Mask Bit Assignment: null = 0, assigned = 1
    * Value Mask Assignment: ######### (e.g. 000001001, 100100111)
    * Value Bit Assignment: Null = 0, X = 0, O = 1           
    * Fields: ######### (e.g. 001011010)
    * Computer Play Level Bit Assignment: Easy = 00, Medium = 01, Difficult = 10, Learning = 11
    * Computer Play Level: ## + Fields (e.g. 00001011010, 01001011010, 10001011010, 11001011010)
    */

    [ServiceContract(SessionMode = SessionMode.Required)]
    public interface IGameEngine
    {
        // Define operation contract for setting GameOptions
        [OperationContract(IsOneWay = false, IsInitiating = true, IsTerminating = false)]
        [FaultContract(typeof(Exception))]
        void PlayLevel(string playLevel);

        // Define operation contract for getting current Game State
        [OperationContract(IsOneWay = false, IsInitiating = false, IsTerminating = false)]
        [FaultContract(typeof(Exception))]
        void ValueMask(string valueMask);

        // Define operation contract for setting Game State
        [OperationContract(IsOneWay = false, IsInitiating = false, IsTerminating = false)]
        [FaultContract(typeof(Exception))]
        void FieldValues(string fieldValues);

        // Define operation contract for setting Game State
        [OperationContract(IsOneWay = false, IsInitiating = false, IsTerminating = false)]
        [FaultContract(typeof(Exception))]
        string GameState();
        // Define operation contract for setting Game State
        [OperationContract(IsOneWay = false, IsInitiating = false, IsTerminating = true)]
        [FaultContract(typeof(Exception))]
        void WinnerStatus(int? winnerStatus);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        // Define Data Member Property for Computer Play Level
        [DataMember]
        public string playLevel { get; set; }

        // Define Data Member Property for Value Mask        
        [DataMember]
        public string valueMask { get; set; }

        // Define Data Member Property for Field Values        
        [DataMember]
        public string fieldValues { get; set; }

        // Define Data Member Property for Winner Status
        [DataMember]
        public int? winnerStatus { get; set; }
    }

}
