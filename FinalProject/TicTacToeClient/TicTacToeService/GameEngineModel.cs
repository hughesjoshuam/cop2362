﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Xml.Serialization;

namespace TicTacToeService
{
    [Serializable]
    public class GameEngineModel
    {
        private string[] _gameStates = null;
        public string[] GameStates
        {
            get { return _gameStates; }
            set
            {
                _gameStates = value;
            }
        }
        public void Save()
        {
            string filename = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "GameEngineModel.xml");
            using (var stream = File.Open(filename, FileMode.Create))
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(GameEngineModel));
                    serializer.Serialize(stream, this);
                }
                catch (System.IO.IOException e)
                {
                    //Implement Fault Contracts -> Later
                }
            }

        }
        public static GameEngineModel Create()
        {
            string filename = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "GameEngineModel.xml");
            if (File.Exists(filename))
            {
                using (var stream = File.OpenRead(filename))
                {
                    try
                    {
                        var serializer = new XmlSerializer(typeof(GameEngineModel));
                        return serializer.Deserialize(stream) as GameEngineModel;
                    }
                    catch (System.IO.IOException e)
                    {
                        //Implement Fault Contracts -> Later
                        return new GameEngineModel();
                    }
                }
            }
            else
                return new GameEngineModel();

        }
    }
}