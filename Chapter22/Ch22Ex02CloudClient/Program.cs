﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Ch22Ex02Contracts;
using static System.Console;
using System.ServiceModel.Description; //Added for credential management
using System.ServiceModel.Channels;

namespace Ch22Ex02Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] people = new Person[]
            {
                new Person { Mark = 46, Name = "Jim" },
                new Person { Mark = 73, Name = "Mike" },
                new Person { Mark = 92, Name = "Stefan" },
                new Person { Mark = 24, Name = "Arthur" }
            };
            WriteLine("People: ");
            OutputPeople(people);
            /*
            IAwardService client = ChannelFactory<IAwardService>.CreateChannel(
                new WSHttpBinding(),
                new EndpointAddress("http://chh22ex02cloudservice.cloudapp.net/AwardService.svc"));
            */

            //Instantiate ChannelFactory Client
            ChannelFactory<IAwardService> client = new ChannelFactory<IAwardService>();
                        
            //Set client EndPointAddress
            EndpointAddress endPointAddress = new EndpointAddress("http://chh22ex02cloudservice.cloudapp.net/AwardService.svc");
            client.Endpoint.Address = endPointAddress;

            //Set client Binding
            Binding binding = new WSHttpBinding();
            client.Endpoint.Binding = binding;

            //Set ClientCredentials for Windows Authentication
            var clientCredentials = client.Credentials;
            clientCredentials.Windows.ClientCredential.UserName = "WCFUser";
            clientCredentials.Windows.ClientCredential.Password = "!_WCFPassword_!";
            
            IAwardService clientProxy = client.CreateChannel();


            clientProxy.SetPassMark(70);
            Person[] awardedPeople = clientProxy.GetAwardedPeople(people);
            WriteLine();
            WriteLine("Awarded people:");
            OutputPeople(awardedPeople);
            ReadKey();
        }
        static void OutputPeople(Person[] people)
        {
            foreach (Person person in people)
            {
                WriteLine("{0}, mark: {1}", person.Name, person.Mark);
            }
        }
    }
}
