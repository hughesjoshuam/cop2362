﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ch22Ex01Client.ServiceReference1;
using System.ServiceModel;
using static System.Console;

namespace Ch22Ex01Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Title = "Ch22Ex01Client";
            string numericInput = null;
            int intParam;
            do
            {
                WriteLine("Enter an integer and press enter to call the WCF service.");
                numericInput = ReadLine();
            }
            while (!int.TryParse(numericInput, out intParam));
            IService1 client = ChannelFactory<IService1>.CreateChannel(
                new BasicHttpBinding(),
                new EndpointAddress("http://ch22cloudservice.cloudapp.net/Service1.svc"));
            WriteLine(client.GetData(intParam));
            WriteLine("Press any key to exit...");
            ReadKey();
        }
    }
}
