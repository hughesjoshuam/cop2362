﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using static System.Console;

namespace Ch21_1_CodeFirstDatabase
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        [Key] public int Code { get; set; }
    }
    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BookContext())
            {
                /*
                Book book1 = new Book { Title = "Beginning Visual C# 2015", Author = "Perkins, Reid, and Hammer" };
                db.Books.Add(book1);
                Book book2 = new Book { Title = "Beginning XML", Author = "Fawcett, Quin, and Ayers" };
                db.Books.Add(book2);
                */

                WriteLine("Add a Book \n");
                Write("Enter Title: ");
                string strTitle = ReadLine();
                Write("\nEnter Author: ");
                string strAuthor = ReadLine();

                Book book1 = new Ch21_1_CodeFirstDatabase.Book { Title = strTitle, Author = strAuthor };

                if(db.Books.Where(b => b.Title == strTitle).Count() > 0)
                {
                    WriteLine("Existing Title. The book is not added to the database. \n");
                } else
                {
                    WriteLine("Adding book to the database. \n");
                    db.Books.Add(book1);
                    db.SaveChanges();
                }

                var query = from b in db.Books
                            orderby b.Title
                            select b;

                WriteLine("\nAll books in the database:");
                foreach (var b in query)
                {
                    WriteLine("{0} by {1}, code={2}", b.Title, b.Author, b.Code);
                }

                WriteLine("Press a key to exit...");
                ReadKey();
            }
        }
    }
}
