﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using static System.Console;

namespace Chapter_21_Exercise_4
{
    public class Story
    {
        [Key] public int StoryID { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public string AuthorNationality { get; set; }
        public string Rating { get; set; }
    }
    public class StoriesContext : DbContext
    {
        public DbSet<Story> Stories { get; set; }
    }
    class Program
    {
        private const string GhostStoriesFile = @"C:\Development\COP2362\Chapter21\Chapter 21 Exercise 4\bin\Debug\GhostStories.xml";
        static void Main(string[] args)
        {
            
            using (var db = new StoriesContext())
            {
                // Load the XML document       
                XDocument GhostStories = XDocument.Load(GhostStoriesFile);

                // Retrieve XML Node values from descendant nodes
                var Stories = from s in GhostStories.Descendants("story")
                              select new
                              {
                                  strTitle = s.Element("title").Value,
                                  strName = s.Element("author").Element("name").Value,
                                  strNationality = s.Element("author").Element("nationality").Value,
                                  strRating = s.Element("rating").Value
                              };

                foreach (var item in Stories)
                {
                    Console.WriteLine("{0}, {1}, {2}, {3}", item.strTitle, item.strName, item.strNationality, item.strRating);
                    //Instantiate story with values parsed from XML Document
                    Story story = new Story { Title = item.strTitle, AuthorName = item.strName, AuthorNationality = item.strNationality, Rating = item.strRating };
                    WriteLine("Adding '{0}' to the database. \n", item.strTitle);

                    //Add story to database
                    db.Stories.Add(story);
                }      
                               
                //Save changes to db
                db.SaveChanges();

                // Query database to display contents
                var query = from s in db.Stories
                            orderby s.Title
                            select s;

                WriteLine("\nAll books in the database:");
                foreach (var s in query)
                {
                    WriteLine("{0} by {1}, Nationality={2}, Rating={3}", s.Title, s.AuthorName, s.AuthorNationality, s.Rating);
                }

                WriteLine("Press a key to exit...");
                ReadKey();

            }
        }
    }
}
