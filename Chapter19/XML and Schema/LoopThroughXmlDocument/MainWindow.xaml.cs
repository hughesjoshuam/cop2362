﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.IO;
using Microsoft.Win32;

namespace LoopThroughXmlDocument
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        //private const string booksfile = @"C:\Development\COP2362\Chapter19\XML and Schema\Books.xml";
        private const string booksjson = @"C:\Development\COP2362\Chapter19\XML and Schema\Books.json";

        public MainWindow()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            InitializeComponent();
        }

        private string FormatText(XmlNode node, string text, string indent)
        {
            if (node is XmlText)
            {
                text += node.Value;
                return text;
            }
            if (string.IsNullOrEmpty(indent))
            {
                indent = " ";
            }
            else
            {
                text += "\r\n" + indent;
            }
            if (node is XmlComment)
            {
                text += node.OuterXml;
                return text;
            }
            text += "<" + node.Name;
            if (node.Attributes.Count > 0)
            {
                AddAttributes(node, ref text);
            }
            if (node.HasChildNodes)
            {
                text += ">";
                foreach (XmlNode child in node.ChildNodes)
                {
                    text = FormatText(child, text, indent + "  ");
                }
                if (node.ChildNodes.Count == 1 && (node.FirstChild is XmlText || node.FirstChild is XmlComment))
                    text += "</" + node.Name + ">";
                else
                    text += "\r\n" + indent + "</" + node.Name + ">";
            }
            else
                text += " />";
            return text;
        }
        private void AddAttributes(XmlNode node, ref string text)
        {
            foreach (XmlAttribute xa in node.Attributes)
            {
                text += " " + xa.Name + "='" + xa.Value + "'";
            }
        }

        private void buttonLoop_Click(object sender, RoutedEventArgs e)
        {
            XmlDocument document = new XmlDocument();
            document.Load(locationTextBox.Text);
            textBlockResults.Text = FormatText(document.DocumentElement as XmlNode, "", "");
        }

        private void buttonCreateNode_Click(object sender, RoutedEventArgs e)
        {
            // Load the XML document
            XmlDocument document = new XmlDocument();
            document.Load(locationTextBox.Text);

            //Get the root element
            XmlElement root = document.DocumentElement;

            // Create the new nodes
            XmlElement newBook = document.CreateElement("book");
            XmlElement newTitle = document.CreateElement("title");
            XmlElement newAuthor = document.CreateElement("author");
            XmlElement newCode = document.CreateElement("code");
            XmlElement newPages = document.CreateElement("pages");
            XmlText title = document.CreateTextNode("Beginning Visual C# 2015");
            XmlText author = document.CreateTextNode("Karli Watson et al");
            XmlText code = document.CreateTextNode("314418");
            XmlText pages = document.CreateTextNode("1000");
            XmlComment comment = document.CreateComment("The previous edition");

            // Insert the elemets
            newBook.AppendChild(comment);
            newBook.AppendChild(newTitle);
            newBook.AppendChild(newAuthor);
            newBook.AppendChild(newCode);
            newBook.AppendChild(newPages);
            newBook.AppendChild(title);
            newAuthor.AppendChild(author);
            newCode.AppendChild(code);
            newPages.AppendChild(pages);
            root.InsertAfter(newBook, root.LastChild);
            document.Save(locationTextBox.Text);
        }

        private void buttonDeleteNode_Click(object sender, RoutedEventArgs e)
        {
            // Load the XML document
            XmlDocument document = new XmlDocument();
            document.Load(locationTextBox.Text);

            // Get the root element
            XmlElement root = document.DocumentElement;

            // Find the node. root is the <books> tag, so its last child which will be the last <book> node.
            if (root.HasChildNodes)
            {
                XmlNode book = root.LastChild;
                
                // Delete the child.
                root.RemoveChild(book);

                // Save the document back to disk.
                document.Save(locationTextBox.Text);
            }
        }

        private void buttonXMLtoJSON_Click(object sender, RoutedEventArgs e)
        {
            // Load the XML Document
            XmlDocument document = new XmlDocument();
            document.Load(locationTextBox.Text);

            string json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(document);

            textBlockResults.Text = json;
            System.IO.File.AppendAllText(booksjson, json);
            
        }
        private void buttonJSONtoXML_Click(object sender, RoutedEventArgs e)
        {

            // Load the JSON Document
            string json = System.IO.File.ReadAllText(booksjson);

            XmlDocument document = Newtonsoft.Json.JsonConvert.DeserializeXmlNode(json);
            textBlockResults.Text = FormatText(document.DocumentElement as XmlNode, "", "");
        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = "xml";
            if (dialog.ShowDialog(this) == true)
            {
                locationTextBox.Text = dialog.FileName;
            }
        }
    }
}
