﻿/*
 * Joshua M. Hughes
 * COP2362
 * Professor Barrell
 * 1/10/2017
 * 
 * Ch12 Ex01
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Ch12Ex01
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector v1 = GetVector("vector1");
            Vector v2 = GetVector("vector2");
            WriteLine($"{v1} + {v2} = {v1 + v2}");
            WriteLine($"{v1} - {v2} = {v1 - v2}");
            ReadKey();
        }
        static Vector GetVector(string name)
        {
            WriteLine($"Input {name} magnitude:");
            double? r = GetNullabledouble();
            WriteLine($"Input {name} angle (in degrees):");
            double? theta = GetNullabledouble();
            return new Vector(r, theta);
        }
        static double? GetNullabledouble()
        {
            double? result;
            string userINput = ReadLine();
            try
            {
                result = double.Parse(userINput);
            }
            catch
            {
                result = null;
            }
            return result;
        }
    }
}
