﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DataSerialization
{
    [DataContract]
    public enum AppStates
    {
        [EnumMember]
        Started,
        [EnumMember]
        Suspended,
        [EnumMember]
        Closing
    }

    [DataContract]
    class AppData
    {
        [DataMember]
        public int TheAnswer { get; set; }
        [DataMember]
        public AppStates State { get; set; }
        [DataMember]
        public object Statedata { get; set; }
    }
}
