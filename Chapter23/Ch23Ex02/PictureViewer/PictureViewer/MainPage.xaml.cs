﻿using System;
using System.Collections.Generic;
using Windows.Storage;
using Windows.Storage.Search;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PictureViewer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private IList<ImageProperties> imageProperties = new List<ImageProperties>();
        private async void GetFiles()
        {
            try
            {
                StorageFolder picturesFolder = KnownFolders.PicturesLibrary;
                IReadOnlyList<StorageFile> sortedItems = await picturesFolder.GetFilesAsync(CommonFileQuery.OrderByDate);
                var images = new List<BitmapImage>();
                if (sortedItems.Any())
                {
                    foreach(StorageFile file in sortedItems)
                    {
                        if (file.FileType.ToUpper() == ".JPG")
                        {
                            using(Windows.Storage.Streams.IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.Read))
                            {
                                BitmapImage bitmapImage = new BitmapImage();
                                await bitmapImage.SetSourceAsync(fileStream);
                                images.Add(bitmapImage);
                                imageProperties.Add(new ImageProperties
                                {
                                    FileName = file.DisplayName,
                                    Height = bitmapImage.PixelHeight,
                                    Width = bitmapImage.PixelWidth
                                });
                                if (imageProperties.Count > 10)
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    var message = new MessageDialog("There are no images in the Pictures Library.");
                    await message.ShowAsync();
                }
                flipView.ItemsSource = images;
            }
            catch (UnauthorizedAccessException)
            {
                var message = new MessageDialog("The app does not have access to the Pictures Library on this device.");
                await message.ShowAsync();
            }
        }
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void flipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(flipView.SelectedIndex >= 0)
            {
                textBlockCurrentImageDisplayName.Text = imageProperties[flipView.SelectedIndex].FileName;
                textBlockCurrentImageImageHeight.Text = imageProperties[flipView.SelectedIndex].Height.ToString();
                textBlockCurrentImageImageWidth.Text = imageProperties[flipView.SelectedIndex].Width.ToString();
            }
        }

        private void Page_Loaded(FrameworkElement sender, object args)
        {
            GetFiles();
        }
    }
}
