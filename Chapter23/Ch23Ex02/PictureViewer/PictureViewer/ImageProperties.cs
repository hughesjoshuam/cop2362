﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureViewer
{
    class ImageProperties
    {
        public string FileName { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
