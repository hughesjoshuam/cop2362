﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using static System.Console;

namespace Ch18Ex05
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set Variables
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop).ToString();
            string fileName = "MyTemporaryFileWithACoolName.txt";
            string strFilePathName = filePath + "/" + fileName;
            string strData = "I had a little monkey, \n\tI took him to the country, \n\tand I fed him on gingerbread. \nAlong came a choo choo \n\tand knocked my monkey coo koo, \n\tand now my monkey's dead...";

            try
            {
                StringBuilder sourceStringMultiplier = new StringBuilder(strData.Length * 100);
                for (int i = 0; i < 100; i++)
                {
                    sourceStringMultiplier.Append(strData);
                }
                strData = sourceStringMultiplier.ToString();
                WriteLine($"Source data is {strData.Length} bytes long.");
                SaveCompressedFile(strFilePathName, strData);
                WriteLine($"\n Data saved to {strFilePathName}.");
                FileInfo compressedFileData = new FileInfo(strFilePathName);
                Write($"Compressed file is {compressedFileData.Length}");
                WriteLine("bytes long.");
                string recoveredString = LoadCompressedFile(strFilePathName);
                recoveredString = recoveredString.Substring(0, recoveredString.Length / 100);
                WriteLine($"\nrecovered data: {recoveredString}");
                ReadKey();
            }
            catch (IOException e)
            {
                WriteLine("An IO exception has been thrown!");
                WriteLine(e.ToString());
                ReadKey();
                return;
            }
            ReadKey();

        }
        static void SaveCompressedFile(string filename, string data)
        {
            FileStream fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write);
            GZipStream compressionStream = new GZipStream(fileStream, CompressionMode.Compress);
            StreamWriter writer = new StreamWriter(compressionStream);
            writer.Write(data);
            writer.Close();
        }
        static string LoadCompressedFile(string filename)
        {
            FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
            GZipStream compressionStream = new GZipStream(fileStream, CompressionMode.Decompress);
            StreamReader reader = new StreamReader(compressionStream);
            string data = reader.ReadToEnd();
            reader.Close();
            return data;
        }
    }
}
