﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using static System.Console;

namespace Ch18E04
{
    class Program
    {
        static void Main(string[] args)
        { 
            // Set Variables
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop).ToString();
            string fileName = "MyTemporaryFileWithACoolName.txt";
            FileMode fileMode = FileMode.Open;

            string line;
            try
            {
                FileStream aFile = new FileStream(filePath + "/" + fileName, fileMode);
                StreamReader sr = new StreamReader(aFile);
                line = sr.ReadLine();
                // Read data in line by line
                while(line != null){
                    WriteLine(line);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (IOException e)
            {
                WriteLine("An IO exception has been thrown!");
                WriteLine(e.ToString());
                ReadKey();
                return;
            }
            ReadKey();
        }
    }
}
