﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Compression;
using Microsoft.Win32;

namespace Ch18Ex06
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // File System Watcher object.
        private FileSystemWatcher watcher;
        public MainWindow()
        {
            InitializeComponent();
            watcher = new FileSystemWatcher();
            watcher.Deleted += (s, e) => AddMessage($"File: {e.FullPath} Deleted.");
            watcher.Renamed += (s, e) => onRename(s, e);
            watcher.Changed += (s, e) => onChange(s, e);
            watcher.Created += (s, e) => onCreate(s, e);
        }
        private void onRename(object s, RenamedEventArgs e)
        {
            AddMessage($"File renamed from {e.OldName} to {e.FullPath}.");
        }
        private void onChange(object s, FileSystemEventArgs e)
        {
            AddMessage($"File: {e.FullPath} {e.ChangeType.ToString()}");
        }
        private void onCreate(object s, FileSystemEventArgs e)
        {
            // Display message that file is added
            AddMessage($"File: {e.FullPath} Created.");

            // Call compression method if .txt file exists
            if (System.IO.Path.GetExtension(e.FullPath) == ".txt")
            {
                AddMessage($"File: {e.FullPath} is a text file and will be compressed.");
                SaveCompressedFile(e.FullPath);
            }
        }
        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {

            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LocationBox.Text = dialog.SelectedPath;
            }
        }
       
        private void btnRadioOn_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                watcher.Path = LocationBox.Text;
                watcher.Filter = "*.*";
                watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.Size | NotifyFilters.DirectoryName;
                AddMessage("Watching " + LocationBox.Text);

                // Begin watching
                watcher.EnableRaisingEvents = true;

            } catch (SystemException systemException)
            {
                MessageBox.Show(systemException.Message.ToString(), systemException.Source.ToString());
                LocationBox.Focus();
                btnRadioOff.IsChecked = true;
                return;
            }
        }

        private void btnRadioOff_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                // Stop watching
                watcher.EnableRaisingEvents = false;
            }
            catch (NullReferenceException nullReferenceException)
            {
                return;
            }
            
        }
        private void LocationBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnRadioOn.IsEnabled = !string.IsNullOrEmpty(LocationBox.Text);
        }
        private void AddMessage(string message)
        {
            Dispatcher.BeginInvoke(new Action(() => Watchoutput.Items.Insert(0, message)));
        }
        static void SaveCompressedFile(string FileName)
        {
            try
            {
                // Read text file contents
                FileStream readStream = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                StreamReader streamReader = new StreamReader(readStream);
                string data = streamReader.ReadToEnd();
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                streamReader.Close();
                readStream.Close();

                // Write file contents to compressed zip file
                string path = System.IO.Path.GetDirectoryName(FileName);
                string filename = System.IO.Path.GetFileName(FileName);
                string zipfile = path + "\\" + filename + ".gz";
                FileStream fileStream = new FileStream(zipfile, FileMode.Create, FileAccess.Write);
                GZipStream compressionStream = new GZipStream(fileStream, CompressionMode.Compress, true);
                compressionStream.Write(buffer, 0, buffer.Length);
                compressionStream.Close();
                fileStream.Close();
                
            } catch (System.IO.IOException e)
            {
                // On access exception, sleep two seconds and try again.
                System.Threading.Thread.Sleep(1000);
                SaveCompressedFile(FileName);
            }
        }
    }
}
