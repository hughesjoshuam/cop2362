﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using static System.Console;

namespace Ch18Ex01
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "../../Program.cs";
            int intBytes = 450;
            int startByte = 5;
            int endByte = 325;
            byte[] byteData = new byte[intBytes];
            char[] charData = new char[intBytes];
            try
            {
                FileStream aFile = new FileStream(fileName, FileMode.Open);
                aFile.Seek(174, SeekOrigin.Begin);
                aFile.Read(byteData, 0, intBytes);
            }
            catch(IOException e)
            {
                WriteLine("An IO exception has been thrown!");
                WriteLine(e.ToString());
                ReadKey();
                return;
            }
            Decoder d = Encoding.UTF8.GetDecoder();
            d.GetChars(byteData, startByte, endByte, charData, 0);
            WriteLine(charData);
            ReadKey();
        }
    }
}
