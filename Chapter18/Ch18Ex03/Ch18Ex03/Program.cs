﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using static System.Console;

namespace Ch18Ex03
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set Variables
            string strData = "I had a little monkey, \n\tI took him to the country, \n\tand I fed him on gingerbread. \nAlong came a choo choo \n\tand knocked my monkey coo koo, \n\tand now my monkey's dead...";
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop).ToString();
            string fileName = "MyTemporaryFileWithACoolName.txt";
            FileMode fileMode = FileMode.OpenOrCreate;
            
            try
            {
                FileStream aFile = new FileStream(filePath + "/" + fileName, fileMode);
                StreamWriter sw = new StreamWriter(aFile);
                bool truth = true;
                // Write data to file.
                sw.WriteLine("Hello to you.");
                sw.Write($"It is now {DateTime.Now.ToLongDateString()}");
                sw.Write(" and things are looking good.");
                sw.Write(" \n\tMore than that");
                sw.Write($" it's Mondo Mega {truth} that C# is fun.\n");
                sw.WriteLine("-----------------------------------------------------------------\n");
                sw.WriteLine(strData);
                sw.Close();
            }catch(IOException e)
            {
                WriteLine("An IO exception has been thrown!");
                WriteLine(e.ToString());
                ReadKey();
                return;
            }

            WriteLine($"Check your desktop for {fileName}.");
            ReadKey();
        }
    }
}
