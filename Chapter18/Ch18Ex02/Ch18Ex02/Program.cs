﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using static System.Console;

namespace Ch18Ex02
{
    class Program
    {
        static void Main(string[] args)
        {
            string strData = "I had a little monkey, \n\tI took him to the country, \n\tand I fed him on gingerbread. \nAlong came a choo choo \n\tand knocked my monkey coo koo, \n\tand now my monkey's dead...";
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop).ToString();
            string fileName = "MyTemporaryFileWithACoolName.txt";
            FileMode fileMode = FileMode.Create;
            int startByte = 0;
            int endByte = 300;
            byte[] byteData;
            char[] charData;
            try
            {
                FileStream aFile = new FileStream(filePath + "/" + fileName, fileMode);
                charData = strData.ToCharArray();
                byteData = new byte[charData.Length];
                Encoder e = Encoding.UTF8.GetEncoder();
                // Check to see if endByte is set greater than the total number of available chars. 
                if(endByte > charData.Length)
                {
                    endByte = charData.Length;
                }

                e.GetBytes(charData, startByte, endByte, byteData, 0, true);
                
                // Move file pointer to beginning of file.
                aFile.Seek(0, SeekOrigin.Begin);
                aFile.Write(byteData, 0, byteData.Length);
            }
            catch (IOException e)
            {
                WriteLine("An IO exception has been thrown!");
                WriteLine(e.ToString());
                ReadKey();
                return;
            }
            WriteLine($"Check your desktop for {fileName}.");
            ReadKey();
        }
    }
}
