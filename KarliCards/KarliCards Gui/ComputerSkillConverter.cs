﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Ch13CardLib;

namespace KarliCards_Gui
{
    [ValueConversion(typeof(ComputerSkillLevel), typeof(string))]
    public class ComputerSkillConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strParameter = parameter.ToString();
            if (strParameter == "dumbAIRadioButton" && (ComputerSkillLevel)value == ComputerSkillLevel.Dumb)
                return true;
            else if (strParameter == "goodAIRadioButton" && (ComputerSkillLevel)value == ComputerSkillLevel.Good)
                return true;
            else if (strParameter == "cheatingAIRadioButton" && (ComputerSkillLevel)value == ComputerSkillLevel.Cheats)
                return true;
            return false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strParameter = parameter.ToString();
            string strValue = value.ToString();
            if (strParameter == "dumbAIRadioButton" && (bool)value == true)
                return ComputerSkillLevel.Dumb;
            else if (strParameter == "goodAIRadioButton" && (bool)value == true)
                return ComputerSkillLevel.Good;
            else if (strParameter == "cheatingAIRadioButton" && (bool)value == true)
                return ComputerSkillLevel.Cheats;
            return false;
        }
    }
}
